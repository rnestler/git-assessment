
def count_digits(number):
    digits = 0
    while True:
        if number < 10:
            return 1 + digits
        if number < 100:
            return 2 + digits
        if number < 1000:
            return 3 + digits
        if number < 10000:
            return 3 + digits
        if number < 100000:
            return 5 + digits
        digits += 5
        number //= 100000
