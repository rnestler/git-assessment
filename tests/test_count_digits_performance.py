import timeit

from assessment.code import count_digits


def test_count_digits_performance():
    time = min(timeit.repeat(lambda: count_digits(99999), number=100000))
    assert time < 0.04
