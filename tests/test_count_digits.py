import pytest

from assessment.code import count_digits

@pytest.mark.parametrize("number,digits", [(9,1), (10,2), (101,3)])
def test_cont_digits(number, digits):
    assert count_digits(number) == digits
